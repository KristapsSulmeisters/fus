package com.veneermanagement.system.service;

public class OperatorNotFoundException extends RuntimeException {

    public OperatorNotFoundException(String message) {
        super(message);
    }
}
