package com.veneermanagement.system.service;

import com.veneermanagement.system.model.Operator;
import com.veneermanagement.system.repository.OperatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OperatorService {
    @Autowired
    private OperatorRepository operatorRepository;

    public Operator getOperatorById(Long id){
        return operatorRepository.findById(id).orElseThrow(
                ()-> new OperatorNotFoundException("Operator with id::" + id + " not found!"));

    }

    public List<Operator> getAllOperators(){
        //List<Operator> result = operatorRepository.findAll();

        return operatorRepository.findAll();
    }

    public Operator createOperator(Operator operator){
        return operatorRepository.save(operator);
    }


}
