package com.veneermanagement.system.controller;

import com.veneermanagement.system.model.Operator;
import com.veneermanagement.system.repository.OperatorRepository;
import com.veneermanagement.system.service.OperatorNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/v1")
public class OperatorController {
    @Autowired
    OperatorRepository operatorRepository;

    @GetMapping("/operators")
    public List<Operator> getAllOperators() {
        return operatorRepository.findAll();
    }

    @GetMapping("/operators/{id}")
    public ResponseEntity<Operator> getOperatorById(
            @PathVariable Long id
    ) throws OperatorNotFoundException {

        Operator operator = operatorRepository.findById(id)
                .orElseThrow(()->new OperatorNotFoundException("Operator with id::" + id + " was not found!"));
        return ResponseEntity.ok().body(operator);

    }



    @PostMapping("/operators")
    public Operator createOperator(@RequestBody Operator operator){
        return operatorRepository.save(operator);
    }

    //update
    @PutMapping("/operators/{id}")
    public ResponseEntity<Operator> updateOperator(
            @PathVariable Long id,
            @PathVariable Operator operatorDetails
    ) throws OperatorNotFoundException{
        Operator operator = operatorRepository.findById(id)
                .orElseThrow(()->new OperatorNotFoundException("Operator with id::" + id + " was not found!"));
        operator.setFirstName(operatorDetails.getFirstName());
        operator.setLastName(operatorDetails.getLastName());
        operator.setIsActive(operatorDetails.getIsActive());
        operator.setKeyCode(operatorDetails.getKeyCode());

        final Operator updatedOperator = operatorRepository.save(operatorDetails);

        return ResponseEntity.ok(updatedOperator);

    }

    @DeleteMapping("operators/{id}")
    public Map<String, Boolean> deleteOperator(@PathVariable Long opId )
    throws OperatorNotFoundException{
        Operator operator = operatorRepository.findById(opId)
                .orElseThrow(()->new OperatorNotFoundException("Operator not found with id:: "+ opId));
        operatorRepository.delete(operator);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
