package com.veneermanagement.system.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table
public class VeneerPack {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String barCode;

    @Column
    private Long widthInMM;

    @Column
    private Long lengthInMM;

    @Column
    private Boolean isActive;

    @Column
    private Long sheetCountInPack;


    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "operator_id" )
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Operator operator;

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public Long getWidthInMM() {
        return widthInMM;
    }

    public void setWidthInMM(Long widthInMM) {
        this.widthInMM = widthInMM;
    }

    public Long getLengthInMM() {
        return lengthInMM;
    }

    public void setLengthInMM(Long lengthInMM) {
        this.lengthInMM = lengthInMM;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Long getSheetCountInPack() {
        return sheetCountInPack;
    }

    public void setSheetCountInPack(Long sheetCountInPack) {
        this.sheetCountInPack = sheetCountInPack;
    }

    @Override
    public String toString() {
        return "VeneerPack{" +
                "id=" + id +
                ", barCode='" + barCode + '\'' +
                ", widthInMM=" + widthInMM +
                ", lengthInMM=" + lengthInMM +
                ", isActive=" + isActive +
                ", sheetCountInPack=" + sheetCountInPack +
                '}';
    }
}
